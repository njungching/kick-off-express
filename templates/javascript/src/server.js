import express from 'express';

export const app = express();
app.get('/', (req, res) => {
  res.send('hello world');
});
export const start = async () => {
  try {
    app.listen(3000, () => {
      console.log(`REST API on http://localhost:3000/`);
    });
  } catch (e) {
    console.error(e);
  }
};
