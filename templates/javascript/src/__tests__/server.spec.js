import request from 'supertest';
import { app } from '../server';

describe('hello world api', () => {
  test('api should return "hello world" ', async () => {
    const response = await request(app).get('/');
    expect(response.text).toBe('hello world');
  });
});
