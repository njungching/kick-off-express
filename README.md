# kick-off-express

[![pipeline status](https://gitlab.com/njungching/kick-off-express/badges/master/pipeline.svg)](https://gitlab.com/njungching/kick-off-express/commits/master)
[![npm](https://img.shields.io/npm/dm/kick-off-express)](https://www.npmjs.com/package/kick-off-express)
[![NPM](https://img.shields.io/npm/l/kick-off-express)](https://gitlab.com/njungching/kick-off-express/blob/master/LICENSE)
[![coverage report](https://gitlab.com/njungching/kick-off-express/badges/master/coverage.svg)](https://gitlab.com/njungching/kick-off-express/commits/master)


A CLI to bootstrap new express projects.

A good starting point for your new express project. Don't worry about setting-up tooling and configurations. This tool will set-up and configure:

- nodemon - monitors any changes in your source and automatically restarts your server.
- babel - converts new JavaScript syntax / next generation JavaScript into a backwards compatible version.
- prettier - enforce code-formatting rules.
- eslint - encforce code-quality rules. Airbnb style guide configured.
- jest - testing framework

## Create an App

### npx

```
npx kick-off-express my-app
```

This will generate the project structure below:

```
my-app
.
+-- .prettierrc
+-- package.json
+-- jest.config.js
+-- .eslintrc.json
+-- .babelrc
+-- src
|   +-- index.js
|   +-- server.js
|   +-- __tests__
+-- package.lock.json
+-- node_modules
```

## Development

`npm start` runs the app in development mode. Open http://localhost:3000.

## Production

Precompile assets using `npm run build`. Use `npm run serve` to start the server in production.

## Testing

Jest is configured and a sample test has been written. Run tests using `npm test`.

## License

[MIT](LICENSE)
