import path from 'path';
import { createProjectDirectory } from './create-dir';
import { executeTasks } from './execute-tasks';

export const kickOffExpress = async (appDir) => {
  const templateDir = path.resolve(__dirname, '../templates', 'javascript');
  createProjectDirectory(appDir);
  executeTasks(appDir, templateDir);
};
