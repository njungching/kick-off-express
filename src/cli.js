import { kickOffExpress } from './main';
import { logPrompt } from './log-prompt';

export const parseArguments = (rawArgs) => {
  return rawArgs[2] || false;
};

export const cli = (args) => {
  const folderName = parseArguments(args);
  if (!folderName) {
    logPrompt();
  } else {
    kickOffExpress(folderName);
  }
};
