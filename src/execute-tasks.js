import chalk from 'chalk';
import Listr from 'listr';
import { projectInstall } from 'pkg-install';
import { copyFilesToProjectDirectory } from './copy-files';

export const executeTasks = async (appDir, templateDir) => {
  const tasks = new Listr([
    {
      title: 'Copy project files',
      task: () => copyFilesToProjectDirectory(appDir, templateDir)
    },
    {
      title: 'Install dependencies',
      task: () =>
        projectInstall({
          cwd: appDir
        })
    }
  ]);
  try {
    await tasks.run();
    console.log(
      '%s Project set-up is successful',
      chalk.green.bold('COMPLETE')
    );
  } catch (err) {
    console.error(`%s ${err} `, chalk.red.bold('ERROR'));
  }
};
