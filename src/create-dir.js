import fs from 'fs';
import chalk from 'chalk';

export const createProjectDirectory = (appDir) => {
  try {
    fs.mkdirSync(appDir);
    console.log(
      '%s Project directory created successfully',
      chalk.green.bold('DONE')
    );
  } catch (err) {
    console.error(`%s ${err} `, chalk.red.bold('ERROR'));
    process.exit(1);
  }
};
