import ncp from 'ncp';
import { promisify } from 'util';

const copy = promisify(ncp);

export const copyFilesToProjectDirectory = (appDir, templateDir) => {
  return copy(templateDir, appDir, {
    clobber: false
  });
};
