import chalk from 'chalk';
import * as install from 'pkg-install';
import { executeTasks } from '../execute-tasks';
import * as copy from '../copy-files';

copy.copyFilesToProjectDirectory = jest.fn();
install.projectInstall = jest.fn();
jest.mock('pkg-install');
describe('execute tasks', () => {
  test('complete message should be logged', async () => {
    const log = jest.spyOn(global.console, 'log');
    await executeTasks();
    expect(log).toHaveBeenCalledWith(
      '%s Project set-up is successful',
      chalk.green.bold('COMPLETE')
    );
  });
  test('error message should be logged', async () => {
    install.projectInstall.mockImplementation(() => {
      throw new Error('Error occured');
    });
    await executeTasks();
    expect(install.projectInstall).toThrow(Error);
  });
});
