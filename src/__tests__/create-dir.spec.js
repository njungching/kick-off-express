import fs from 'fs';
import chalk from 'chalk';
import { createProjectDirectory } from '../create-dir';

jest.mock('fs');

describe('create directory', () => {
  test('fs should be called', () => {
    createProjectDirectory('my-express-app');
    expect(fs.mkdirSync).toHaveBeenCalled();
  });
  test('success message should be logged', () => {
    const log = jest.spyOn(global.console, 'log');
    createProjectDirectory('my-express-app');
    expect(log).toHaveBeenCalledWith(
      '%s Project directory created successfully',
      chalk.green.bold('DONE')
    );
  });
  test('error message should be logged', async () => {
    const mockExit = jest.spyOn(process, 'exit').mockImplementation(() => {});
    fs.mkdirSync.mockImplementation(() => {
      throw new Error('Error occured');
    });
    createProjectDirectory('my-express-app');
    expect(mockExit).toHaveBeenCalled();
    expect(fs.mkdirSync).toThrow(Error);
    mockExit.mockRestore();
  });
});
