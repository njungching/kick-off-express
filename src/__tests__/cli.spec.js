import { parseArguments, cli } from '../cli';
import * as log from '../log-prompt';
import * as main from '../main';

log.logPrompt = jest.fn();
main.kickOffExpress = jest.fn();

describe('cli', () => {
  const args1 = ['/usr/local/bin/node', '/usr/local/bin/kick-off-express'];
  const args2 = [
    '/usr/local/bin/node',
    '/usr/local/bin/kick-off-express',
    'my-express-app'
  ];
  test('when project directory is not provided should return false', () => {
    const result = parseArguments(args1);
    expect(result).toBe(false);
  });
  test('when project directory is not provided should call logPrompt()', () => {
    cli(args1);
    expect(log.logPrompt).toHaveBeenCalledTimes(1);
  });
  test('when project directory is provided should return directory name', () => {
    const result = parseArguments(args2);
    expect(result).toBe('my-express-app');
  });
  test('when project directory is provided should call kickOffExpress()', () => {
    cli(args2);
    expect(main.kickOffExpress).toHaveBeenCalledTimes(1);
  });
});
