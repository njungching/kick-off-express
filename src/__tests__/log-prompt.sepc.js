import { logPrompt } from '../log-prompt';

global.console = {
  log: jest.fn()
};
describe('logPrompt()', () => {
  test('create directory messages should be logged', () => {
    logPrompt();
    expect(global.console.log).toHaveBeenCalledTimes(4);
  });
});
