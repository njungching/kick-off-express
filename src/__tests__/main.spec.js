import { kickOffExpress } from '../main';
import * as create from '../create-dir';
import * as execute from '../execute-tasks';

create.createProjectDirectory = jest.fn();
execute.executeTasks = jest.fn();
const args = [
  '/usr/local/bin/node',
  '/usr/local/bin/kick-off-express',
  'my-express-app'
];
describe('main', () => {
  test('createProjectDirectory should be called', () => {
    kickOffExpress(args);
    expect(create.createProjectDirectory).toHaveBeenCalled();
  });
  test('executeTasks should be called', () => {
    kickOffExpress(args);
    expect(execute.executeTasks).toHaveBeenCalled();
  });
});
