import ncp from 'ncp';
import { copyFilesToProjectDirectory } from '../copy-files';

jest.mock('ncp');
describe('copy files', () => {
  test('ncp should be called', () => {
    copyFilesToProjectDirectory();
    expect(ncp).toHaveBeenCalled();
  });
});
