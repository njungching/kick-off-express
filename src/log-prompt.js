import chalk from 'chalk';

export const logPrompt = () => {
  console.log('Please specify the project directory:');
  console.log(
    `  ${chalk.blueBright('kick-off-express ')}${chalk.green(
      '<project-directory> \n'
    )}`
  );

  console.log('For example:');
  console.log(
    `  ${chalk.blueBright('kick-off-express ')}${chalk.green('my-express-app')}`
  );
};
